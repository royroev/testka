import { push } from 'connected-react-router';

import createReducer from '../../utils/reducers';
import serialize from '../../utils/serialize';

export const SORT_BY = 'sorting/SORT_BY';

const initialState = {
  params: {},
};

const onSortBy = (state = initialState, action) => ({ ...state, params: action.params });

export const sortByAction = params => (dispatch) => {
  dispatch({ type: SORT_BY, params });

  dispatch(push(`/?${serialize(params)}`));
};

export default createReducer(
  {
    [SORT_BY]: onSortBy,
  },
  initialState,
);
