import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { reducer as formReducer } from 'redux-form';

import books from './books';
import sorting from './sorting';

export default history => combineReducers({
  router: connectRouter(history),
  books,
  sorting,
  form: formReducer,
});
