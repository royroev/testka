import { push } from 'connected-react-router';
import createReducer from '../../utils/reducers';

export const ADD_BOOK = 'books/ADD_BOOK';
export const REMOVE_BOOK = 'books/REMOVE_BOOK';

const initialState = {
  items: [],
};

const onSubmitForm = (state = initialState, { values }) => {
  const items = [].concat(state.items);

  if (values.id !== undefined) {
    const findItemIndex = items.findIndex(({ id }) => id === values.id);
    if (findItemIndex !== -1) {
      items[findItemIndex] = values;

      return { ...state, items };
    }
  }

  const book = { ...values, id: items.length > 0 ? items[items.length - 1].id + 1 : 0 };

  items.push(book);

  return { ...state, items };
};

const onRemoveItem = (state = initialState, action) => (
  { ...state, items: state.items.filter(({ id }) => id !== action.id) }
);

export const submitFormAction = values => (dispatch) => {
  dispatch({ type: ADD_BOOK, values });
};

export const deleteItemAction = id => (dispatch) => {
  dispatch({ type: REMOVE_BOOK, id });

  dispatch(push('/'));
};

export default createReducer(
  {
    [ADD_BOOK]: onSubmitForm,
    [REMOVE_BOOK]: onRemoveItem,
  },
  initialState,
);
