/* global window */
import { createStore, applyMiddleware, compose } from 'redux';
import { routerMiddleware } from 'connected-react-router';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import thunk from 'redux-thunk';
import createHistory from 'history/createBrowserHistory';

import createRootReducer from './reducers';

export const history = createHistory();

const persistConfig = {
  key: 'root',
  storage,
  blacklist: ['form'],
};

const initialState = {};
const enhancers = [];
const middleware = [
  thunk,
  routerMiddleware(history),
];

if (process.env.NODE_ENV === 'development') {
  const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__; // eslint-disable-line

  if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension());
  }
}

const composedEnhancers = compose(
  applyMiddleware(...middleware),
  ...enhancers,
);

const persistedReducer = persistReducer(persistConfig, createRootReducer(history));

export default () => {
  const store = createStore(
    persistedReducer,
    initialState,
    composedEnhancers,
  );
  const persistor = persistStore(store);

  return { store, persistor };
};
