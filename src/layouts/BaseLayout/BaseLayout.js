import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import styles from './BaseLayout.css';

const BaseLayout = ({ children }) => (
  <Fragment>
    <header className={styles.header}>
      <nav className={styles.navBar}>
        <div>
          <h1>Книги</h1>
        </div>
        <ul className={styles.menuItems}>
          <li>
            <Link to="/" className={styles.link}>Домой</Link>
          </li>
          <li>
            <Link to="/add-book" className={styles.link}>Добавить новую книгу</Link>
          </li>
        </ul>
      </nav>
    </header>
    <div className={styles.container}>
      {children}
    </div>
  </Fragment>
);

BaseLayout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default BaseLayout;
