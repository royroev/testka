import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

const mapStateToProps = ({ books: { items }, sorting: { params = {} } = {} }) => {
  const { sortBy, orderBy } = params;

  return {
    items,
    sortBy,
    orderBy,
  };
};

export default (Target) => {
  class BooksListContainer extends PureComponent {
    render() {
      return <Target {...this.props} />;
    }
  }

  return connect(mapStateToProps)(BooksListContainer);
};
