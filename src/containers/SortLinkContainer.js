import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import { sortByAction } from '../redux/reducers/sorting';

const mapStateToProps = ({ sorting: { params = {} } = {} }) => params;

export default (Target) => {
  class SortBlockContainer extends PureComponent {
    render() {
      return <Target {...this.props} />;
    }
  }

  return connect(mapStateToProps, { sortByAction })(SortBlockContainer);
};
