import React, { PureComponent } from 'react';
import { matchPath } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { submitFormAction, deleteItemAction } from '../redux/reducers/books';
import { bookItemPropTypes } from '../components/BookItem';

const mapStateToProps = ({ books: { items } }) => ({ items });

export default (Target) => {
  class ContainerWrapper extends PureComponent {
    propTypes = {
      location: PropTypes.shape.isRequired,
      onDelete: PropTypes.func.isRequired,
      items: PropTypes.shape([
        bookItemPropTypes,
      ]).isRequired,
    };

    render() {
      const {
        onDelete, location, items = [], ...rest
      } = this.props;

      const props = {
        ...rest,
      };

      const editParams = matchPath(location.pathname, {
        path: '/edit-book/:id',
        exact: false,
        strict: false,
      });

      if (editParams) {
        const itemId = parseInt(editParams.params.id, 10);
        props.initialValues = items.find(({ id }) => id === itemId);
        props.onDelete = () => onDelete(itemId);
      }

      return <Target {...props} />;
    }
  }

  return connect(mapStateToProps, {
    onSubmit: submitFormAction,
    onDelete: deleteItemAction,
  })(ContainerWrapper);
};
