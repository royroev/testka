import React from 'react';
import PropTypes from 'prop-types';

import styles from './InputField.css';

const propTypes = {
  input: PropTypes.shape,
  name: PropTypes.string,
  label: PropTypes.string,
  type: PropTypes.string,
  meta: PropTypes.shape({
    touched: PropTypes.bool,
    error: PropTypes.string,
  }),
};

const InputField = ({
  input, name, label, type, meta: { touched, error },
}) => (
  <div className={styles.formGroup}>
    <label htmlFor={name} className={styles.label}>
      {label}
    </label>
    <div>
      <input
        {...input}
        id={name}
        type={type}
        className={styles.formControl}
        placeholder={label}
      />
      {touched && error && <span className={styles.error}>{error}</span>}
    </div>
  </div>
);

InputField.propTypes = propTypes;
InputField.defaultProps = {
  type: 'text',
  input: null,
  name: null,
  label: null,
  meta: null,
};

export default InputField;
