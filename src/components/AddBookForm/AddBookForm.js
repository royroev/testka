import React from 'react';
import {
  Field, reduxForm, FieldArray, Form,
} from 'redux-form';
import PropTypes from 'prop-types';

import Container from '../../containers/AddBookFormContainer';
import CustomInputField from '../InputField';
import FileInput from '../FileInput';
import RenderMembers from './RenderMembers';
import validate from './validate';
import styles from './AddBookForm.css';

const AddBookForm = ({
  submitting, handleSubmit, pristine, initialValues, onDelete,
}) => (
  <div className={styles.formContainer}>
    <Form onSubmit={handleSubmit} className={styles.form}>
      <Field label="Название книги" name="title" component={CustomInputField} />
      <FieldArray name="authors" component={RenderMembers} />
      <Field label="Кол-во страниц" name="pages" component={CustomInputField} type="number" />
      <Field label="Название издательства" name="company" component={CustomInputField} />
      <Field label="Год публикации" name="year" component={CustomInputField} type="number" />
      <Field label="Дата выхода в тираж" name="publishDate" component={CustomInputField} type="date" />
      <Field label="ISBN" name="isbn" component={CustomInputField} />
      <Field label="Изображение" name="img" type="file" component={FileInput} />

      <button type="submit" disabled={pristine || submitting} className={styles.btn}>
        Сохранить
      </button>
    </Form>
    {
      initialValues && (
        <button type="button" className={styles.btn} onClick={onDelete}>
          Удалить
        </button>
      )
    }
  </div>
);

AddBookForm.propTypes = {
  submitting: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  pristine: PropTypes.bool.isRequired,
  initialValues: PropTypes.shape,
  onDelete: PropTypes.func.isRequired,
};
AddBookForm.defaultProps = {
  initialValues: null,
};

const createReduxForm = reduxForm({ form: 'addBookForm', validate });

export default Container(createReduxForm(AddBookForm));
