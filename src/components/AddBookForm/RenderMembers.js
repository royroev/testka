import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';

import styles from './AddBookForm.css';

import CustomInputField from '../InputField/InputField';

const renderSubFields = (author, index, fields) => (
  <div key={index} className={styles.subFieldsContainer}>
    <h4>
      Автор #
      {index + 1}
    </h4>
    <div className={styles.subFields}>
      <Field
        name={`${author}.firstName`}
        type="text"
        component={CustomInputField}
        label="Имя"
      />
      <Field
        name={`${author}.lastName`}
        type="text"
        component={CustomInputField}
        label="Фамилия"
      />
    </div>
    {fields.length > 1 && (
      <button
        type="button"
        title="Удалить"
        onClick={() => fields.remove(index)}
      >
        Удалить
      </button>
    )}
  </div>
);

const RenderAuthors = ({ fields, meta: { error, submitFailed } }) => (
  <div className={styles.authors}>
    <h3>Авторы:</h3>
    {fields.map(renderSubFields)}
    <button type="button" onClick={() => fields.push({})} className={styles.btn}>
      Добавить автора
    </button>
    {submitFailed && error && <span className={styles.error}>{error}</span>}
  </div>
);

RenderAuthors.propTypes = {
  fields: PropTypes.shape.isRequired,
  meta: PropTypes.shape.isRequired,
};

export default RenderAuthors;
