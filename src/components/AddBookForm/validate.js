const Required = 'Поле обязательно для заполнения';

const checkDate = (date) => {
  const regExp = new RegExp('^(\\d{4})-(\\d{2})-(\\d{2})$'); /* ex. 2019-01-02 */

  if (regExp.test(date)) {
    const matches = date.match(regExp);
    return parseInt(matches[1], 10) < 1800
      || parseInt(matches[2], 10) < 1
      || parseInt(matches[3], 10) < 1;
  }

  return false;
};

const checkISBN = isbn => (!new RegExp('^(97(8|9))?\\d{9}(\\d|X)$').test(isbn));

const validate = (values) => {
  const errors = {};

  if (!values.title) {
    errors.title = Required;
  } else if (values.title.length > 30) {
    errors.title = 'Не более 30 символов';
  }

  if (!values.authors || !values.authors.length) {
    errors.authors = { _error: 'Вы должны добавить хотя бы одного автора' };
  } else {
    const authorsArrayErrors = [];
    values.authors.forEach((author, authorIndex) => {
      const authorErrors = {};
      if (!author || !author.firstName) {
        authorErrors.firstName = Required;
        authorsArrayErrors[authorIndex] = authorErrors;
      } else if (author.firstName.length > 20) {
        authorErrors.firstName = 'Не более 20 символов';
        authorsArrayErrors[authorIndex] = authorErrors;
      }
      if (!author || !author.lastName) {
        authorErrors.lastName = Required;
        authorsArrayErrors[authorIndex] = authorErrors;
      } else if (author.lastName.length > 20) {
        authorErrors.lastName = 'Не более 20 символов';
        authorsArrayErrors[authorIndex] = authorErrors;
      }
    });
    if (authorsArrayErrors.length) {
      errors.authors = authorsArrayErrors;
    }
  }

  if (!values.pages) {
    errors.pages = Required;
  } else if (!(values.pages > 0 && values.pages < 10000)) {
    errors.pages = 'Больше 0 и не более 10000';
  }

  if (values.company && values.company.length > 30) {
    errors.company = 'Не более 30 символов';
  }

  if (values.year && values.year < 1800) {
    errors.year = 'Не ранее 1800 года';
  }

  if (values.publishDate && checkDate(values.publishDate)) {
    errors.publishDate = 'Не раньше 01.01.1800';
  }

  if (values.isbn && checkISBN(values.isbn)) {
    errors.isbn = 'Не верный isbn';
  }

  return errors;
};

export default validate;
