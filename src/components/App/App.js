import React from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import { PersistGate } from 'redux-persist/integration/react';
import { hot } from 'react-hot-loader/root';

import createStore, { history } from '../../redux/store';
import AppRouter from '../AppRouter';
import styles from './App.css';

const { store, persistor } = createStore();

const App = () => (
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <ConnectedRouter history={history}>
        <div className={styles.wrapper}>
          <div className={styles.content}>
            <AppRouter />
          </div>
          <footer className={styles.footer}>
            <span className={styles.footerText}>RoyRoev 2019 (c)</span>
          </footer>
        </div>
      </ConnectedRouter>
    </PersistGate>
  </Provider>
);

export default hot(App);
