import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './FileInput.css';

const propTypes = {
  input: PropTypes.shape.isRequired,
  resetKey: PropTypes.string.isRequired,
};

class FileInput extends Component {
  propTypes = propTypes;

  state = {
    preview: null,
  };

  onChange = (e) => {
    const { input: { onChange } } = this.props;

    const imgFile = e.target.files[0];
    const reader = new FileReader();

    reader.onload = (event) => {
      const preview = event.target.result;

      this.setState({ preview }, () => {
        onChange(preview.replace(/^data:image\/(png|jpg);base64,/, ''));
      });
    };

    reader.readAsDataURL(imgFile);
  };

  render() {
    const { preview } = this.state;
    const { resetKey, input } = this.props;
    const { value, ...inputProps } = input;

    return (
      <div className={styles.fileInput}>
        <div>
          {preview && <img src={preview} alt="" />}
        </div>
        <input
          {...inputProps}
          key={resetKey}
          type="file"
          onChange={this.onChange}
          onBlur={() => {}}
          accept="image/jpeg, image/png"
        />
      </div>
    );
  }
}

export default FileInput;
