import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import styles from './BookItem.css';

const BookItem = ({
  id,
  title,
  authors,
  pages,
  company,
  year,
  publishDate,
  isbn,
  img,
}) => {
  const style = { backgroundImage: img ? `url(data:image/png;base64,${img})` : '' };

  const config = [
    { title: 'Авторы', text: authors.map(({ firstName, lastName }) => `${firstName} ${lastName}`).join(', ') },
    { title: 'Кол-во страниц', text: pages },
    { title: 'Название издательства', text: company },
    { title: 'Год публикации', text: year },
    { title: 'Дата выхода в тираж', text: publishDate },
    { title: 'ISBN', text: isbn },
  ];

  return (
    <Link to={`/edit-book/${id}`} className={styles.item} style={style}>
      <div className={styles.background}>
        <div className={styles.title}>{title}</div>
        <div className={styles.info}>
          <ul className={styles.infoList}>
            {config.map(({ title: infoTitle, text }) => (text ? (
              <li key={infoTitle}>
                {`${infoTitle}: ${text}`}
              </li>
            ) : null))}
          </ul>
        </div>
      </div>
    </Link>
  );
};

export const propTypes = {
  id: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  authors: PropTypes.arrayOf(PropTypes.shape(
    {
      firstName: PropTypes.string.isRequired,
      lastName: PropTypes.string.isRequired,
    },
  )).isRequired,
  pages: PropTypes.string.isRequired,
  company: PropTypes.string,
  year: PropTypes.string,
  publishDate: PropTypes.string,
  isbn: PropTypes.string,
  img: PropTypes.string,
};

BookItem.propTypes = propTypes;
BookItem.defaultProps = {
  img: null,
  company: null,
  isbn: null,
  publishDate: null,
  year: null,
};

export default BookItem;
