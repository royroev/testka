import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Container from '../../containers/BooksListContainer';
import BookItem, { bookItemPropTypes } from '../BookItem';
import SortBlock from '../SortBlock';
import styles from './BooksList.css';

const BooksList = (props) => {
  const { sortBy, orderBy } = props;
  let { items } = props;

  if (sortBy) {
    items = items.sort((a, b) => {
      const leftParam = orderBy === 'desc' ? a : b;
      const rightParam = orderBy === 'desc' ? b : a;
      if (typeof leftParam[sortBy] === 'string') {
        return (`${leftParam[sortBy]}`).localeCompare(rightParam[sortBy]);
      }
      return leftParam[sortBy] - rightParam[sortBy];
    });
  }

  return items.length ? (
    <div>
      <SortBlock />
      <div className={styles.booksList}>
        {items.map(({
          title, authors = [], pages, company, year, publishDate, startDate, isbn, img, id,
        }) => (
          <BookItem
            key={id}
            id={id}
            title={title}
            authors={authors}
            pages={pages}
            company={company}
            year={year}
            publishDate={publishDate}
            startDate={startDate}
            isbn={isbn}
            img={img}
          />
        ))}
      </div>
    </div>
  ) : (
    <div>
      <Link to="/add-book">
        <h2>Нет добавленных книг, добавьте первую!</h2>
      </Link>
    </div>
  );
};

BooksList.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape(bookItemPropTypes)).isRequired,
  sortBy: PropTypes.string,
  orderBy: PropTypes.string,
};
BooksList.defaultProps = {
  sortBy: null,
  orderBy: null,
};

export default Container(BooksList);
