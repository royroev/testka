import React from 'react';
import { Switch } from 'react-router-dom';
import { Route, BookList, AddBookForm } from '..';

import BaseLayout from '../../layouts/BaseLayout';

const AppRouter = () => (
  <Switch>
    <Route path="/" exact component={BookList} layout={BaseLayout} />
    <Route path="/add-book" component={AddBookForm} layout={BaseLayout} />
    <Route path="/edit-book/:id" component={AddBookForm} layout={BaseLayout} />
  </Switch>
);

export default AppRouter;
