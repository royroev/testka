import React from 'react';
import SortLink from '../SortLink';

import styles from './SortBlock.css';

const SortBlock = () => (
  <div className={styles.sortBlock}>
    <div className={styles.sortBlockTitle}>
      Отсортировать по:
    </div>
    <SortLink param="title" title="Заголовку" />
    <SortLink param="year" title="Году публикации" />
  </div>
);

export default SortBlock;
