import React from 'react';
import PropTypes from 'prop-types';
import { Route } from 'react-router-dom';

const RouteComponent = ({
  layout: Layout, component: Component,
}) => {
  if (Layout) {
    return (
      <Route
        render={props => (
          <Layout {...props}>
            <Component {...props} />
          </Layout>
        )}
      />
    );
  }

  return <Route component={Component} />;
};

RouteComponent.propTypes = {
  component: PropTypes.func.isRequired,
  layout: PropTypes.func.isRequired,
};

export default RouteComponent;
