import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import cn from 'classnames';
import PropTypes from 'prop-types';

import Container from '../../containers/SortLinkContainer';
import styles from './SortLink.css';

const propTypes = {
  param: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  sortByAction: PropTypes.func.isRequired,
  orderBy: PropTypes.string,
  sortBy: PropTypes.string,
};

class SortLink extends PureComponent {
  propTypes = propTypes;

  static defaultProps = {
    sortBy: null,
    orderBy: null,
  };

  sortHandler = (event) => {
    const {
      param, sortByAction, orderBy, sortBy,
    } = this.props;

    const params = {
      sortBy: param,
      orderBy: sortBy === param && orderBy === 'asc' ? 'desc' : 'asc',
    };

    event.preventDefault();

    sortByAction(params);
  };

  render() {
    const {
      sortBy, param, title, orderBy,
    } = this.props;
    const className = cn(styles.sortBlockLink, { [styles.active]: param === sortBy });
    const modifier = orderBy === 'desc' && sortBy === param ? styles.desc : styles.asc;

    return (
      <div className={styles.sortBlockItem}>
        <Link to="/" className={className} onClick={this.sortHandler}>
          {title}
        </Link>
        <div className={cn(styles.orderBy, modifier)} />
      </div>
    );
  }
}

export default Container(SortLink);
